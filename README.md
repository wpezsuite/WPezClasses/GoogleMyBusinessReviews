# WPezClass: Google My Business Reviews

As originally seen here (but refactored to clean up various issues): 

https://christophrado.com/2022/08/fetching-google-my-business-reviews-using-the-google-api-and-oauth2-a-wordpress-boilerplate-class/

The main branch will be the original, at least for now, with the WPez refactoring happening in a different branch.  
